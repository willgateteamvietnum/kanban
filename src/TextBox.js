import React, { forwardRef } from "react"
import styled from "styled-components";

const STextBox = styled.input.attrs({
    type: 'text',
})`
    border: none;
    border-radius: .25rem;
    box-sizing: border-box;
    width: 100%;
    padding: 0.5rem;
    &:focus {
        outline: 0;
    }
`

const TextBox = forwardRef(({
    children,
    ...props
}, ref) => {
    return <STextBox {...props} ref={ref}>{ children }</STextBox>
})

export default TextBox