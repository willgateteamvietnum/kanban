import React, { useState, useRef, useEffect } from "react"
import styled from "styled-components"
import TextBox from "./TextBox"

const SCard = styled.div`
    display: flex;
    flex-direction: column;
    border: 1px solid rgba(0,0,0,.125);
    border-radius: .25rem;
    background: #fff;
    box-shadow: 0 1px 0 rgba(9,30,66,.25);

    padding: 6px 8px;

    cursor: pointer;
    &:hover {
        background: #efefef;
    }
`

const STitle = styled.div`
    padding: 0.5rem;
`

const Card = ({ id, title, colId, editable = false, onChange }) => {

    const [state, setState] = useState({
        value: title,
        isEditable: editable
    })
    const { value, isEditable } = state
    const textBox = useRef(null)

    const editCard = () => {
        setState({
            ...state,
            isEditable: true
        })
    }

    useEffect(() => {
        if (textBox.current) textBox.current.focus()
    }, [isEditable])

    return <SCard>
        {
            isEditable ? (
                <form onSubmit={(e) => {
                    e.preventDefault();
                    setState({
                        ...state,
                        isEditable: false
                    })
                    onChange(colId, id, { title: value })
                }}>
                    <TextBox
                        ref={textBox}
                        defaultValue={value}
                        onChange={(e) => {
                            setState({
                                ...state,
                                value: e.currentTarget.value
                            })
                        }}
                        onBlur={(e) => {
                            setState({
                                ...state,
                                isEditable: false
                            })
                        }}
                    />
                </form>
            ) : (
                <STitle onClick={editCard}>{ value }</STitle>
            )
        }
    </SCard>
}

export default Card