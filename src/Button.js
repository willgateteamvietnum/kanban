import React from "react"
import styled from "styled-components"

const buttonCSS = `
    border: none;
    border-radius: .25rem;
    box-sizing: border-box;
    padding: 0.5rem;
    &:focus {
        outline: 0;
    }

    color: rgba(0,0,0,.5);
    background: transparent;
    &:hover {
        color: rgba(0,0,0,.8);
        background: rgba(0,0,0,.125);
    }
`

const SSubmitButton = styled.input.attrs({
    type: "submit"
})`
    ${buttonCSS}
`

const SButton = styled.button`
    ${buttonCSS}
`

const Button = ({
    type,
    label,
    children,
    ...props
}) => {
    if (type === "submit") {
        return <SSubmitButton value={label} {...props} />
    } else {
        return <SButton {...props} >{ children }</SButton>
    }
}

export default Button