export const createDummyData = (cardNum, colNum) => {
    let cardIdNumber = 1
    let columnIdNumber = 1
    const data = {
        columns: []
    }
    for (let i = 0; i<colNum; i++) {
        const cards = []
        for (let j = 0; j<cardNum; j++) {
            cards.push({
                id: cardIdNumber+j,
                title: `カード${cardIdNumber+j}`,
            })
        }
        cardIdNumber += cardNum
        data.columns.push({
            id: columnIdNumber+i,
            title: `カラム${columnIdNumber+i}`,
            cards
        })
    }
    columnIdNumber += colNum
    return {
        data,
        cardIdNumber,
        columnIdNumber
    }
}