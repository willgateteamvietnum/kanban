import React from "react"
import styled from "styled-components"

const SModal = styled.div`
    position: absolute;
    background: rgba(0,0,0,.5);
    z-index: 100;
    width: 100vw;
    height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;
`

const SModalPannel = styled.div`
    width: 768px;
    box-sizing: border-box;
    padding: 1rem;
    background: #ebecf0;
    border-radius: .25rem;
    display: flex;
    flex-direction: column;
`

const Modal = ({ children, onClose }) => {
    return <SModal onClick={onClose} >
        <SModalPannel onClick={(e) => {
            e.stopPropagation()
        }}>
            { children }
        </SModalPannel>
    </SModal>
}

export default Modal