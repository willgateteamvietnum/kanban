import React, { useState } from "react"
import styled from "styled-components"
import TextBox from "./TextBox"

const SForm = styled.form`
    display: flex;
    flex-direction: column;
`

const SComment = styled.div`
    margin-top: 0.5rem;
    font-size: 0.8rem;
`

const CreateColumn = ({ onCreateColumn, targetColId }) => {

    const [state, setState] = useState({
        title: ''
    })
    const { title } = state

    return <SForm onSubmit={(e) => {
        e.preventDefault()
        onCreateColumn({
            title
        })
    }}>
        <TextBox
            name="title"
            defaultValue={title}
            placeholder="エンターで追加"
            onChange={(e) => {
                setState({
                    ...state,
                    title: e.currentTarget.value
                })
            }}
        />
        <SComment>※枠外をクリックして閉じる</SComment>
    </SForm>
}

export default CreateColumn