import React from "react"
import styled from "styled-components"
import Card from "./Card"
import Button from "./Button"
import { ReactSortable } from "react-sortablejs";

const SCardList = styled.div`
    display: flex;
    flex-direction: column;

    width: 272px;
    margin: 30px 30px 30px 0px;
    padding: 5px;
    box-sizing: border-box;

    cursor: pointer;
    background: #ebecf0;
    border-radius: .25rem;
    & > * {
        margin-bottom: 5px;
    }
`

const SCardListTitle = styled.div`
    font-weight: bold;
    padding: 5px;
`

const CardList = ({ colId, title, cards, onClickCreateCard, setCards, onUpdateCard }) => {
    return (
        <SCardList>
            <SCardListTitle>{title}</SCardListTitle>
            <ReactSortable
                list={cards}
                setList={setCards}
                group="card"
                animation={150}
                style={{ minHeight: 500 }}
            >
                {
                    cards.map((card) => {
                        return (
                            <Card
                                id={card.id}
                                title={ card.title }
                                key={card.id}
                                colId={colId}
                                onChange={onUpdateCard}
                            />
                        )
                    })
                }
            </ReactSortable>
            {
                <Button
                    onClick={() => {
                        onClickCreateCard(colId)
                    }}
                >カードを追加する</Button>
            }
        </SCardList>
    )
}

export default CardList