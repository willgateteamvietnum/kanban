import React, { useState } from 'react'
import styled from 'styled-components'
import './App.css'
import CardList from './CardList'
import Modal from './Modal'
import { ReactSortable } from "react-sortablejs";
import CreateCard from './CreateCard';
import Button from './Button'
import CreateColumn from './CreateColumn';
import { createDummyData } from './utilities'

const data = createDummyData(3, 5)
let cardIdNumber = data.cardIdNumber
let columnIdNumber = data.columnIdNumber
const dummyData = data.data

const SApp = styled.div`
  display: flex;
  overflow: scroll;
`

function App() {

  const [state, setState] = useState({
    columns: dummyData.columns,
    targetColId: -1,
    isShownCreateCardModal: false,
    isShownCreateColumnModal: false,
  })
  const { columns, targetColId, isShownCreateCardModal, isShownCreateColumnModal } = state

  const findColumnById = (id) => columns.find((col) => col.id === id)

  const showCreateCardModal = (colId) => {
    setState({
      ...state,
      targetColId: colId,
      isShownCreateCardModal: true
    })
  }

  const closeCreateCardModal = () => {
    setState({
      ...state,
      targetColId: -1,
      isShownCreateCardModal: false
    })
  }

  const createCard = (colId, cardParams) => {
    if (colId <= 0) return
    findColumnById(colId).cards.push(
      {
        ...cardParams,
        id: cardIdNumber
      }
    )
    cardIdNumber++

    setState({
      ...state,
      columns: [ ...columns]
    })
    closeCreateCardModal()
  }

  const setCardFunctions = columns.map((col, index) => {
    return (newCards) => {
      columns[index].cards = newCards
      setState({
        ...state,
        columns: [ ...columns ]
      })
    }
  })

  const setColumns = (newColumns) => {
    setState({
      ...state,
      columns: newColumns
    })
  }

  const updateCard = (colId, cardId, cardParams) => {
    console.log(colId, cardId, cardParams)
    const column = findColumnById(colId)
    const cardIndex = column.cards.findIndex((card) => card.id === cardId)
    column.cards[cardIndex] = Object.assign(column.cards[cardIndex], cardParams)
  }

  const columnComponents = columns.map((col, index) => {
    return <CardList
      colId={col.id}
      title={col.title}
      cards={col.cards}
      key={col.id}
      setCards={setCardFunctions[index]}
      onClickCreateCard={showCreateCardModal}
      onUpdateCard={updateCard}
    />
  })

  const showCreateColumnModal = () => {
    setState({
      ...state,
      isShownCreateColumnModal: true,
    })
  }

  const closeCreateColumnModal = () => {
    setState({
      ...state,
      isShownCreateColumnModal: false,
    })
  }

  const createColumn = (columnParam) => {
    columns.push({
      id: columnIdNumber++,
      cards: [],
      ...columnParam
    })

    setState({
      ...state,
      columns: [ ...columns ]
    })
    closeCreateColumnModal()
  }

  return (
    <SApp>
      { isShownCreateCardModal ? 
        <Modal
          onClose={closeCreateCardModal}
        >
          <CreateCard
            targetColId={targetColId}
            onCreateCard={createCard}
          />
        </Modal> :<></> }
      { isShownCreateColumnModal ? 
        <Modal
          onClose={closeCreateColumnModal}
        >
          <CreateColumn
            onCreateColumn={createColumn}
          />
        </Modal> :<></> }
      <ReactSortable
        list={columns}
        setList={setColumns}
        group="column"
        animation={150}
        style={{ display: "flex", marginLeft: "30px" }}
      >
        {
          columnComponents
        }
      </ReactSortable>
      <Button
        style={{ margin: "30px 30px 30px 0px", minWidth: "200px" }}
        onClick={showCreateColumnModal}
      >カラムを追加する</Button>
    </SApp>
  );
}

export default App;
